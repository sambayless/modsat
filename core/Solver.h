/****************************************************************************************[Solver.h]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#ifndef Minisat_Solver_h
#define Minisat_Solver_h

#include "mtl/Vec.h"
#include "mtl/Heap.h"
#include "mtl/Alg.h"
#include "mtl/Rnd.h"
#include "utils/Options.h"
#include "core/SolverTypes.h"
#include "core/Theory.h"
#include "core/Config.h"
namespace Minisat {

//=================================================================================================
// Solver -- the main class:

class Solver:public Theory,public Allocated {
public:

    // Constructor/Destructor:
    //
	Solver();
	//Allocate a solver using an external ClauseAllocator.
	//You can use this to allow multiple solvers to share one clause allocation space.
    Solver(ClauseAllocator * external_allocator);
    virtual ~Solver();

    // Problem specification:
    //
    Var     newVar    (bool polarity = true, bool dvar = true); // Add a new variable with parameters specifying variable mode.

    bool    addClause (const vec<Lit>& ps);                     // Add a clause to the solver.
    bool    addEmptyClause();                                   // Add the empty clause, making the solver contradictory.
    bool    addClause (Lit p);                                  // Add a unit clause to the solver. 
    bool    addClause (Lit p, Lit q);                           // Add a binary clause to the solver. 
    bool    addClause (Lit p, Lit q, Lit r);                    // Add a ternary clause to the solver. 
    bool    addClause_(      vec<Lit>& ps);                     // Add a clause to the solver without making superflous internal copy. Will


    //Theory interface
    void addTheory(Theory*t){
    	theories.push(t);
    	cancelUntil(0);
    	resetInitialPropagation();
    }

    //Call to force at least one round of propagation to each theory solver at the next solve() call
    void resetInitialPropagation(){
    	if(!initialPropagate){
			initialPropagate=true;//to force propagation to occur at least once to the theory solvers
			if(S){
				S->resetInitialPropagation();
			}
    	}
    }

    void detatchTheory(Theory*t){
    	int i,j =0;
    	for(i = 0;i<theories.size();i++){
    		if(theories[i]==t){
    			int k,l=0;
    			for(k=0;k<markers.size();k++){
    				CRef cr = markers[k];
    			  	int index = CRef_Undef-cr-1;
    			  	int theory = marker_index[index];
    				if(theory==i){
    					marker_index[index]=-1;
    				}else{
    					markers[l++]=cr;
    					if(marker_index[index]>i){
    						marker_index[index]--;
    					}
    				}
    			}
    			markers.shrink(k-l);
    		}else{
    			theories[j++]=theories[i];
    		}
    	}
    	theories.shrink(i-j);
    	cancelUntil(0);
    }

    //Generate a new, unique `temporary value' for explaining conflicts
    CRef newReasonMarker(Theory * forTheory){
    	markers.push(ca.makeMarkerReference());


    	int marker_num = CRef_Undef-markers.last()-1;
    	marker_index.growTo(marker_num+1,-1);

    	//this could be done more efficiently
    	for(int i = 0;i<theories.size();i++){
    		if(theories[i]==forTheory){
    			marker_index[marker_num]=i;
    			break;
    		}
    	}
    	assert(marker_index[marker_num]>=0);

    	return markers.last();
    }

    //We might move this functionality into the clauseAllocator.
    bool isTheoryMarker(CRef cr){
    	return cr != CRef_Undef && !ca.isClause(cr);
    }

    //We might move this functionality into the clauseAllocator.
    int getTheory(CRef cr){
    	assert(isTheoryMarker(cr));
    	int index = CRef_Undef-cr - 1;
    	return marker_index[index];
    }

    //Construct the reason clause for the assignment of p, on demand, as needed by conflict analysis.
    CRef constructReason(Lit p){
    	CRef cr = reason_unsafe(var(p));
    	assert(isTheoryMarker(cr));
    	assert(!ca.isClause(cr));
    	assert(cr!=CRef_Undef);
    	int t = getTheory(cr);
    	theory_reason.clear();
    	theories[t]->buildTheoryReason(p,theory_reason);
    	CRef reason = ca.alloc(theory_reason, !opt_permanent_theory_conflicts);
    	if(opt_permanent_theory_conflicts)
			clauses.push(reason);
		else
			learnts.push(reason);
    	assert(theory_reason[0]==p); assert(value(p)==l_True);

		attachClause(reason);
		vardata[var(p)]=mkVarData(reason,level(var(p)));
		return reason;
    }

    //Attach this solver to the next, connecting literals in the super solver to literals in this solver
    //Super_vars and local_vars _must_ be consecutive sets of variables in the super solver.
    void attachTo(Solver * super, vec<Var> & super_vars, vec<Var> & local_vars){
    	assert(decisionLevel()==0);
    	S=super;

    	assert(super_vars.size()==local_vars.size());
    	super_qhead=0;
    	local_qhead=0;
		super_offset=super_vars[0]-local_vars[0];
		min_super=super_vars[0];
		max_super=super_vars.last();
		min_local=local_vars[0];
		max_local=local_vars.last();
		cause_marker=S->newReasonMarker(this);
    }
                                                      // change the passed vector 'ps'.

    // Solving:
    //
    bool    simplify     ();                        // Removes already satisfied clauses.
    bool    solve        (const vec<Lit>& assumps); // Search for a model that respects a given set of assumptions.
    lbool   solveLimited (const vec<Lit>& assumps); // Search for a model that respects a given set of assumptions (With resource constraints).
    bool    solve        ();                        // Search without assumptions.
    bool    solve        (Lit p);                   // Search for a model that respects a single assumption.
    bool    solve        (Lit p, Lit q);            // Search for a model that respects two assumptions.
    bool    solve        (Lit p, Lit q, Lit r);     // Search for a model that respects three assumptions.
    bool    okay         () const;                  // FALSE means solver is in a conflicting state

    void    toDimacs     (FILE* f, const vec<Lit>& assumps);            // Write CNF to file in DIMACS-format.
    void    toDimacs     (const char *file, const vec<Lit>& assumps);
    void    toDimacs     (FILE* f, Clause& c, vec<Var>& map, Var& max);

    // Convenience versions of 'toDimacs()':
    void    toDimacs     (const char* file);
    void    toDimacs     (const char* file, Lit p);
    void    toDimacs     (const char* file, Lit p, Lit q);
    void    toDimacs     (const char* file, Lit p, Lit q, Lit r);
    
    // Variable mode:
    // 
    void    setPolarity    (Var v, bool b); // Declare which polarity the decision heuristic should use for a variable. Requires mode 'polarity_user'.
    void    setDecisionVar (Var v, bool b); // Declare if a variable should be eligible for selection in the decision heuristic.

    // Read state:
    //
    lbool   value      (Var x) const;       // The current value of a variable.
    lbool   value      (Lit p) const;       // The current value of a literal.
    lbool   modelValue (Var x) const;       // The value of a variable in the last model. The last call to solve must have been satisfiable.
    lbool   modelValue (Lit p) const;       // The value of a literal in the last model. The last call to solve must have been satisfiable.
    int     nAssigns   ()      const;       // The current number of assigned literals.
    int     nClauses   ()      const;       // The current number of original clauses.
    int     nLearnts   ()      const;       // The current number of learnt clauses.
    int     nVars      ()      const;       // The current number of variables.
    int     nFreeVars  ()      const;

    // Resource contraints:
    //
    void    setConfBudget(int64_t x);
    void    setPropBudget(int64_t x);
    void    budgetOff();
    void    interrupt();          // Trigger a (potentially asynchronous) interruption of the solver.
    void    clearInterrupt();     // Clear interrupt indicator flag.

    // Memory managment:
    //
    virtual void garbageCollect();
    void    checkGarbage(double gf);
    void    checkGarbage();

    // Extra results: (read-only member variable)
    //
    vec<lbool> model;             // If problem is satisfiable, this vector contains the model (if any).
    vec<Lit>   conflict;          // If problem is unsatisfiable (possibly under assumptions),
                                  // this vector represent the final conflict clause expressed in the assumptions.
    vec<vec<Lit> > interpolant;  //This vector represents an interpolant between this module and its super solver ('S'), if it is attached to such a solver and the instance is UNSAT.
    							// Variables in each clause in the interpolant vector are in the super solver's variable space, not the subsolver's.

    // Mode of operation:
    //
    int       verbosity;
    double    var_decay;
    double    clause_decay;
    double    random_var_freq;
    double    random_seed;
    bool      luby_restart;
    int       ccmin_mode;         // Controls conflict clause minimization (0=none, 1=basic, 2=deep).
    int       phase_saving;       // Controls the level of phase saving (0=none, 1=limited, 2=full).
    bool      rnd_pol;            // Use random polarities for branching heuristics.
    bool      rnd_init_act;       // Initialize variable activities with a small random value.
    double    garbage_frac;       // The fraction of wasted memory allowed before a garbage collection is triggered.

    int       restart_first;      // The initial restart limit.                                                                (default 100)
    double    restart_inc;        // The factor with which the restart limit is multiplied in each restart.                    (default 1.5)
    double    learntsize_factor;  // The intitial limit for learnt clauses is a factor of the original clauses.                (default 1 / 3)
    double    learntsize_inc;     // The limit for learnt clauses is multiplied with this factor each restart.                 (default 1.1)

    int       learntsize_adjust_start_confl;
    double    learntsize_adjust_inc;

    vec<Lit>  theory_reason;
    vec<Lit> theory_conflict;

    //Theories attached to this solver
    vec<Theory*> theories;

    //a set of special clause references that can be recognized as pointers to theories, for lazy conflict analysis
    //Since this is a bit fragile, might move this functionality into the clause allocator.
    vec<CRef> markers;
    vec<int> marker_index;

    //S is the 'super solver' - the SAT solver, if any, that this solver is a theory solver for.
    Solver * S;
    bool initialPropagate;//to force propagation to occur at least once to the theory solvers
    //qheads for keeping track of propagations from and to the super solver.
    int super_qhead;
    int local_qhead;
    CRef cause_marker;
    int track_min_level;
    int initial_level;

    //The interface between this solver and its parent solver, S.
    Var max_super;
    Var min_super;
    Var min_local;
    Var max_local;

    //All variables that are shared between this solver and its parent must be at a constant offset between the two solvers.
    //This may be relaxed in the future, as it makes it awkward to apply this to anything but bounded model checking...
    int super_offset;

    // Statistics: (read-only member variable)
    //
    uint64_t solves, starts, decisions, rnd_decisions, propagations, conflicts;
    uint64_t dec_vars, clauses_literals, learnts_literals, max_literals, tot_literals;

protected:

    // Helper structures:
    //
    struct VarData { CRef reason; int level; };
    static inline VarData mkVarData(CRef cr, int l){ VarData d = {cr, l}; return d; }

    struct Watcher {
        CRef cref;
        Lit  blocker;
        Watcher(CRef cr, Lit p) : cref(cr), blocker(p) {}
        bool operator==(const Watcher& w) const { return cref == w.cref; }
        bool operator!=(const Watcher& w) const { return cref != w.cref; }
    };

    struct WatcherDeleted
    {
        const ClauseAllocator& ca;
        WatcherDeleted(const ClauseAllocator& _ca) : ca(_ca) {}
        bool operator()(const Watcher& w) const { return ca[w.cref].mark() == 1; }
    };

    struct VarOrderLt {
        const vec<double>&  activity;
        bool operator () (Var x, Var y) const { return activity[x] > activity[y]; }
        VarOrderLt(const vec<double>&  act) : activity(act) { }
    };

    // Solver state:
    //
    bool                ok;               // If FALSE, the constraints are already unsatisfiable. No part of the solver state may be used!
    vec<CRef>           clauses;          // List of problem clauses.
    vec<CRef>           learnts;          // List of learnt clauses.
    double              cla_inc;          // Amount to bump next clause with.
    vec<double>         activity;         // A heuristic measurement of the activity of a variable.
    double              var_inc;          // Amount to bump next variable with.
    ClauseAllocator  &   ca;
    bool				using_external_allocator;//Whether or not the clause allocator was constructed by (and hence must be destructed by) this solver.
    OccLists<Lit, vec<Watcher>, WatcherDeleted>
                        watches;          // 'watches[lit]' is a list of constraints watching 'lit' (will go there if literal becomes true).
    vec<lbool>          assigns;          // The current assignments.
    vec<char>           polarity;         // The preferred polarity of each variable.
    vec<char>           decision;         // Declares if a variable is eligible for selection in the decision heuristic.
    vec<Lit>            trail;            // Assignment stack; stores all assigments made in the order they were made.
    vec<int>            trail_lim;        // Separator indices for different decision levels in 'trail'.
    vec<VarData>        vardata;          // Stores reason and level for each variable.
    int                 qhead;            // Head of queue (as index into the trail -- no more explicit propagation queue in MiniSat).
    int                 simpDB_assigns;   // Number of top-level assignments since last execution of 'simplify()'.
    int64_t             simpDB_props;     // Remaining number of propagations that must be made before next execution of 'simplify()'.
    vec<Lit>            assumptions;      // Current set of assumptions provided to solve by the user.
    Heap<VarOrderLt>    order_heap;       // A priority queue of variables ordered with respect to the variable activity.
    double              progress_estimate;// Set by 'search()'.
    bool                remove_satisfied; // Indicates whether possibly inefficient linear scan for satisfied clauses should be performed in 'simplify'.



    // Temporaries (to reduce allocation overhead). Each variable is prefixed by the method in which it is
    // used, exept 'seen' wich is used in several places.
    //
    vec<char>           seen;
    vec<Lit>            analyze_stack;
    vec<Lit>            analyze_toclear;
    vec<Lit>            add_tmp;

    double              max_learnts;
    double              learntsize_adjust_confl;
    int                 learntsize_adjust_cnt;

    // Resource contraints:
    //
    int64_t             conflict_budget;    // -1 means no budget.
    int64_t             propagation_budget; // -1 means no budget.
    bool                asynch_interrupt;

    // Main internal methods:
    //
    void     insertVarOrder   (Var x);                                                 // Insert a variable in the decision order priority queue.
    Lit      pickBranchLit    ();                                                      // Return the next decision variable.
    void     newDecisionLevel ();                                                      // Begins a new decision level.
    void     uncheckedEnqueue (Lit p, CRef from = CRef_Undef);                         // Enqueue a literal. Assumes value of literal is undefined.
    bool     enqueue          (Lit p, CRef from = CRef_Undef);                         // Test if fact 'p' contradicts current state, enqueue otherwise.
    CRef     propagateAll        ();                                                   // Perform unit propagation on both this solver's clauses and any subtheories. Returns possibly conflicting clause.
    CRef     propagate        ();                                                      // Perform unit propagation. Returns possibly conflicting clause.
    bool 	propagateTheory(vec<Lit> & conflict);
    bool	solveTheory(vec<Lit> & conflict);
    void 	buildTheoryReason(Lit p, vec<Lit> & reason);
    void 	backtrackTheory(int level);
    void     cancelUntil      (int level);                                             // Backtrack until a certain level.
    void     analyze          (CRef confl, vec<Lit>& out_learnt, int& out_btlevel);    // (bt = backtrack)
    void 	analyzeFinal(CRef confl, Lit skip_lit, vec<Lit>& out_conflict);
    void     analyzeFinal     (Lit p, vec<Lit>& out_conflict);                         // COULD THIS BE IMPLEMENTED BY THE ORDINARIY "analyze" BY SOME REASONABLE GENERALIZATION?
    bool     litRedundant     (Lit p, uint32_t abstract_levels);                       // (helper method for 'analyze()')
    lbool    search           (int nof_conflicts);                                     // Search for a given number of conflicts.
    lbool    solve_           ();                                                      // Main solve method (assumptions given in 'assumptions').
    void     reduceDB         ();                                                      // Reduce the set of learnt clauses.
    void     removeSatisfied  (vec<CRef>& cs);                                         // Shrink 'cs' to contain only non-satisfied clauses.
    void     rebuildOrderHeap ();

    // Maintaining Variable/Clause activity:
    //
    void     varDecayActivity ();                      // Decay all variables with the specified factor. Implemented by increasing the 'bump' value instead.
    void     varBumpActivity  (Var v, double inc);     // Increase a variable with the current 'bump' value.
    void     varBumpActivity  (Var v);                 // Increase a variable with the current 'bump' value.
    void     claDecayActivity ();                      // Decay all clauses with the specified factor. Implemented by increasing the 'bump' value instead.
    void     claBumpActivity  (Clause& c);             // Increase a clause with the current 'bump' value.

    // Operations on clauses:
    //
    void     attachClause     (CRef cr);               // Attach a clause to watcher lists.
    void     detachClause     (CRef cr, bool strict = false); // Detach a clause to watcher lists.
    void     removeClause     (CRef cr);               // Detach and free a clause.
    bool     locked           (const Clause& c) const; // Returns TRUE if a clause is a reason for some implication in the current state.
    bool     satisfied        (const Clause& c) const; // Returns TRUE if a clause is satisfied in the current state.



    // Misc:
    //
    int      decisionLevel    ()      const; // Gives the current decisionlevel.
    uint32_t abstractLevel    (Var x) const; // Used to represent an abstraction of sets of decision levels.
    CRef     reason_unsafe           (Var x) const;
    CRef	reason(Lit p) ;
    int      level            (Var x) const;
    double   progressEstimate ()      const; // DELETE THIS ?? IT'S NOT VERY USEFUL ...
    bool     withinBudget     ()      const;
    bool 	addConflictClause(vec<Lit> & theory_conflict,CRef & confl_out);
    // Static helpers:
    //
    //Interface for converting a child solvers variables to a parent solvers variables.
    inline void toSuper(vec<Lit> & from, vec<Lit> & to){

    	to.growTo(from.size());
    	to.shrink(to.size()- from.size());
		for(int i = 0;i<from.size();i++){
			Lit l = from[i];
			assert(local_interface(l));
			to[i]=mkLit(var(l)+super_offset,sign(l));
			assert(super_interface(to[i]));
		}
    }

    inline Var toSuper(Var p){
		assert(p<nVars());
		assert(local_interface(p));
		Var v=  p+super_offset;
		assert(v<S->nVars());
		assert(super_interface(v));
		return v;
	}

	inline Lit toSuper(Lit p){
		assert(var(p)<nVars());
		assert(local_interface(p));

		Lit l=  mkLit(var(p)+super_offset,sign(p));
		assert(var(l)<S->nVars());
		assert(super_interface(l));
		return l;
	}
	inline Var fromSuper(Var p){
		return  p-super_offset;
	}

	inline Lit fromSuper(Lit p){
		return  mkLit(var(p)-super_offset,sign(p));
	}
	inline bool local_interface(Lit p)const{
			return var(p)>=min_local && var(p) <= max_local;
		}
	  inline bool local_interface(Var v)const{
		return v>=min_local  && v <= max_local;
	  }
	inline bool super_interface(Lit p)const{
		return var(p)>=min_super && var(p) <= max_super;
	  }
	inline bool super_interface(Var v)const{
		return v>=min_super  && v <= max_super;
	}

public:
        void     relocAll         (ClauseAllocator& to);
        ClauseAllocator & getClauseAllocator(){
        	return ca;
        }
};


//=================================================================================================
// Implementation of inline methods:
inline CRef Solver::reason(Lit p) {
    if(isTheoryMarker(reason_unsafe(var(p)))){
     	//lazily construct the reason for this theory propagation now that we need it, by constructing the reason in the theory solver.
     	 constructReason(p);//this reason is also set in vardata[x].reason, so future calls to reason_unsafe will return the newly constructed clause
     }
	return reason_unsafe(var(p));
}
inline CRef Solver::reason_unsafe(Var x) const { return vardata[x].reason; }
inline int  Solver::level (Var x) const { return vardata[x].level; }

inline void Solver::insertVarOrder(Var x) {
    if (!order_heap.inHeap(x) && decision[x]) order_heap.insert(x); }

inline void Solver::varDecayActivity() { var_inc *= (1 / var_decay); }
inline void Solver::varBumpActivity(Var v) { varBumpActivity(v, var_inc); }
inline void Solver::varBumpActivity(Var v, double inc) {
    if ( (activity[v] += inc) > 1e100 ) {
        // Rescale:
        for (int i = 0; i < nVars(); i++)
            activity[i] *= 1e-100;
        var_inc *= 1e-100; }

    // Update order_heap with respect to new activity:
    if (order_heap.inHeap(v))
        order_heap.decrease(v); }

inline void Solver::claDecayActivity() { cla_inc *= (1 / clause_decay); }
inline void Solver::claBumpActivity (Clause& c) {
        if (c.learnt() && (c.activity() += cla_inc) > 1e20 ) {
            // Rescale:
            for (int i = 0; i < learnts.size(); i++)
                ca[learnts[i]].activity() *= 1e-20;
            cla_inc *= 1e-20; } }

inline void Solver::checkGarbage(void){ return checkGarbage(garbage_frac); }
inline void Solver::checkGarbage(double gf){
    if (ca.wasted() > ca.size() * gf)
        garbageCollect(); }

// NOTE: enqueue does not set the ok flag! (only public methods do)
inline bool     Solver::enqueue         (Lit p, CRef from)      { return value(p) != l_Undef ? value(p) != l_False : (uncheckedEnqueue(p, from), true); }
inline bool     Solver::addClause       (const vec<Lit>& ps)    { ps.copyTo(add_tmp); return addClause_(add_tmp); }
inline bool     Solver::addEmptyClause  ()                      { add_tmp.clear(); return addClause_(add_tmp); }
inline bool     Solver::addClause       (Lit p)                 { add_tmp.clear(); add_tmp.push(p); return addClause_(add_tmp); }
inline bool     Solver::addClause       (Lit p, Lit q)          { add_tmp.clear(); add_tmp.push(p); add_tmp.push(q); return addClause_(add_tmp); }
inline bool     Solver::addClause       (Lit p, Lit q, Lit r)   { add_tmp.clear(); add_tmp.push(p); add_tmp.push(q); add_tmp.push(r); return addClause_(add_tmp); }
inline bool     Solver::locked          (const Clause& c) const { return value(c[0]) == l_True && ca.isClause( reason_unsafe(var(c[0]))) && ca.lea(reason_unsafe(var(c[0]))) == &c; }
inline void     Solver::newDecisionLevel()                      { trail_lim.push(trail.size());}

inline int      Solver::decisionLevel ()      const   { return trail_lim.size(); }
inline uint32_t Solver::abstractLevel (Var x) const   { return 1 << (level(x) & 31); }
inline lbool    Solver::value         (Var x) const   { return assigns[x]; }
inline lbool    Solver::value         (Lit p) const   { return assigns[var(p)] ^ sign(p); }
inline lbool    Solver::modelValue    (Var x) const   { return model[x]; }
inline lbool    Solver::modelValue    (Lit p) const   { return model[var(p)] ^ sign(p); }
inline int      Solver::nAssigns      ()      const   { return trail.size(); }
inline int      Solver::nClauses      ()      const   { return clauses.size(); }
inline int      Solver::nLearnts      ()      const   { return learnts.size(); }
inline int      Solver::nVars         ()      const   { return vardata.size(); }
inline int      Solver::nFreeVars     ()      const   { return (int)dec_vars - (trail_lim.size() == 0 ? trail.size() : trail_lim[0]); }
inline void     Solver::setPolarity   (Var v, bool b) { polarity[v] = b; }
inline void     Solver::setDecisionVar(Var v, bool b) 
{ 
    if      ( b && !decision[v]) dec_vars++;
    else if (!b &&  decision[v]) dec_vars--;

    decision[v] = b;
    insertVarOrder(v);
}
inline void     Solver::setConfBudget(int64_t x){ conflict_budget    = conflicts    + x; }
inline void     Solver::setPropBudget(int64_t x){ propagation_budget = propagations + x; }
inline void     Solver::interrupt(){ asynch_interrupt = true; }
inline void     Solver::clearInterrupt(){ asynch_interrupt = false; }
inline void     Solver::budgetOff(){ conflict_budget = propagation_budget = -1; }
inline bool     Solver::withinBudget() const {
    return !asynch_interrupt &&
           (conflict_budget    < 0 || conflicts < (uint64_t)conflict_budget) &&
           (propagation_budget < 0 || propagations < (uint64_t)propagation_budget); }

// FIXME: after the introduction of asynchronous interrruptions the solve-versions that return a
// pure bool do not give a safe interface. Either interrupts must be possible to turn off here, or
// all calls to solve must return an 'lbool'. I'm not yet sure which I prefer.
inline bool     Solver::solve         ()                    { budgetOff(); assumptions.clear(); return solve_() == l_True; }
inline bool     Solver::solve         (Lit p)               { budgetOff(); assumptions.clear(); assumptions.push(p); return solve_() == l_True; }
inline bool     Solver::solve         (Lit p, Lit q)        { budgetOff(); assumptions.clear(); assumptions.push(p); assumptions.push(q); return solve_() == l_True; }
inline bool     Solver::solve         (Lit p, Lit q, Lit r) { budgetOff(); assumptions.clear(); assumptions.push(p); assumptions.push(q); assumptions.push(r); return solve_() == l_True; }
inline bool     Solver::solve         (const vec<Lit>& assumps){ budgetOff(); assumps.copyTo(assumptions); return solve_() == l_True; }
inline lbool    Solver::solveLimited  (const vec<Lit>& assumps){ assumps.copyTo(assumptions); return solve_(); }
inline bool     Solver::okay          ()      const   { return ok; }

inline void     Solver::toDimacs     (const char* file){ vec<Lit> as; toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p){ vec<Lit> as; as.push(p); toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p, Lit q){ vec<Lit> as; as.push(p); as.push(q); toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p, Lit q, Lit r){ vec<Lit> as; as.push(p); as.push(q); as.push(r); toDimacs(file, as); }


//=================================================================================================
// Debug etc:


//=================================================================================================
}

#endif
